smodels.resources package
=========================

Submodules
----------

smodels.resources.resource_registry module
------------------------------------------

.. automodule:: smodels.resources.resource_registry
    :members:
    :undoc-members:
    :show-inheritance:

smodels.resources.wsgi_web module
---------------------------------

.. automodule:: smodels.resources.wsgi_web
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: smodels.resources
    :members:
    :undoc-members:
    :show-inheritance:
