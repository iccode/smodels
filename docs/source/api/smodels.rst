smodels package
===============

smodels is an aggregation of tools/methods that we commonly use in our application.

It provides
    * Convenient methods around SqlAlchemy
    * A wsgi_app that provides a REST-like API for the declared objects
    * A session wrapper that acts as a context



Subpackages
-----------

.. toctree::

    smodels.resources
    smodels.session
    smodels.utils
    smodels.versioning

Submodules
----------

smodels.caching module
----------------------

.. automodule:: smodels.caching
    :members:
    :undoc-members:
    :show-inheritance:

smodels.sa module
-----------------

.. automodule:: smodels.sa
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: smodels
    :members:
    :undoc-members:
    :show-inheritance:
