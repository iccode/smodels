from setuptools import setup, find_packages

setup(name='smodels',
      version='2.5.0',
      description='Models and REST infrastructure',
      author='Tasos Vogiatzoglou',
      author_email='tvoglou@iccode.gr',
      packages = find_packages(),
      package_data = {},
      install_requires=[
          "sqlalchemy==0.9.4",
          "i18n",
          "dateutils",
          "inflect",
          "dogpile.cache",
          'redis',
          'werkzeug'
      ],
)
