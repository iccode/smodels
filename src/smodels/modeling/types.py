from sqlalchemy import Column
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy.sql import ColumnElement, text

class ArrayElement(ColumnElement):
    def contains(self,value,**kw):
        if isinstance(value,basestring):
            return text("%s @> ARRAY['%s']"%(self,value))
        if isinstance(value,list):
            return text("%s && ARRAY[%s]"%(self,",".join(["'%s'"%s for s in value])))
        else:
            return text("%s @> ARRAY[%s]"%(self,value))

class ModelColumn(Column):
    def __init__(self, *args, **kwargs):
        super(ModelColumn, self).__init__(*args, **kwargs)

    @property
    def pk(self):
        self.primary_key = True
        return self.required

    @property
    def autopk(self):
        self.primary_key = True
        self.autoincrement = True
        return self.required

    @property
    def required(self):
        self.nullable = False
        return self

class ArrayColumn(ArrayElement,Column):
    pass

def Array(type,*args,**kw):
    if type is Text:
        return ArrayColumn(postgresql.ARRAY(sa.Text),*args,**kw)

def Boolean(name,*args,**kw):
    return Column(name,sa.Boolean,*args,**kw)

def Text(*args,**kwargs):
    if "max" in kwargs:
        max = int(kwargs.pop('max'))
        return Column(sa.String(max),*args,**kwargs)

    return Column(sa.Text,*args,**kwargs)

def Integer(name,*args,**kw):
    return Column(name,sa.Integer,*args,**kw)

def Decimal(name,*args,**kw):
    return Column(name,sa.Numeric,*args,**kw)

def StringChoice(*args,**kw):
    choices = kw.pop('choices',[])
    multiple = kw.pop('multiple',False)
    if not multiple:
        return Column(sa.Text,*args,**kw)
    else:
        return Array(Text,*args,**kw)

def Dictionary(name=None, *args, **kw):
    if name is None:
        return HSTORE(*args, **kw)
    else:
        return HSTORE(name, args, **kw)
