
DB_URL = lambda namespace: None
"""
A callable that must return a db url corresponding to the passed namespace
"""

NS_PROVIDER = lambda : ""
"""
A callable that must return the current namespace or "" if None
"""

CACHE_REGIONS = {}
"""
A dictionary with cache regions
"""

