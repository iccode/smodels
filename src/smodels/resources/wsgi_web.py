import json
import logging
import datetime
from decimal import Decimal
from sqlalchemy.exc import DataError, IntegrityError
from werkzeug.exceptions import NotFound, Conflict, Forbidden, HTTPException

from . import resource_registry, UnsupportedActionError, ResourceNotFoundError
from werkzeug.wrappers import Request, Response
from .resource_registry import _ResourceInstance
from ..session import DbSession
from smodels.sa import SAEntity


def representation(obj, view='default'):
    if hasattr(obj, 'representation'):
        return obj.representation(view)
    elif isinstance(obj, list):
        return [representation(r, view) for r in obj]
    elif isinstance(obj, dict):
        ret = {k: representation(v, view) for k, v in obj.iteritems()}
        return ret
    else:
        return obj


def encoder(obj):
    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()
    if isinstance(obj, Decimal):
        return float(obj)

    return obj


def encode_json(obj, indent=True, view='default'):
    """

    :param obj:
    :param indent:
    :param view:
    :return:
    :rtype:
    :raise:
    """
    obj = representation(obj, view)
    return json.dumps(obj, indent=indent, default=encoder)

class ResourcesApp():
    def __init__(self, pre_hook = None, post_hook=None):
        self.log = logging.getLogger('resources.api')
        self.__pre_hook = pre_hook
        self.__post_hook = post_hook

    def _handle_GET(self, request, _namespace, path):
        with DbSession(namespace=_namespace):
            try:
                view = request.args.get('_view', 'default')
                return representation(resource_registry.get(path, request), view)
            except DataError, e:
                raise NotFound(description=e.message)
            except ResourceNotFoundError:
                raise NotFound(description=path)

    def _handle_POST(self, request, _namespace, path):
        with DbSession(namespace=_namespace) as sess:

            try:
                res, add = resource_registry.post(path, request)
                print res, add
                if add:
                    sess.add(res)
                sess.commit()

                return representation(res)
            except ResourceNotFoundError, err:
                raise NotFound()
            except IntegrityError, err:
                self.log.error(err)
                raise Conflict(err.message)
            except UnsupportedActionError, err:
                raise Forbidden(err.message)
        pass

    def _handle_PUT(self, request, _namespace, path):
        with DbSession(namespace=_namespace) as sess:
            try:
                self.log.debug("Searching for resource %s", path)
                res = resource_registry.put(path, request)
                if isinstance(res, _ResourceInstance):
                    if res.isNew:
                        sess.add(res.instance)
                    sess.commit()

                    return representation(res.instance)

                return representation(res)
            except IntegrityError, err:
                raise Conflict(err.message)
            except UnsupportedActionError, err:
                raise Forbidden(err.message)

    def _handle_DELETE(self, request, _namespace, path):
        print "Namespace is", _namespace
        with DbSession(namespace=_namespace) as sess:
            ret = resource_registry.delete(path, request)
            print "Deleting ", ret
            sess.delete(ret)
            sess.commit()
            return "ok"

    def wsgi_app(self, environ, start_response):

        if self.__pre_hook and not self.__pre_hook(environ):
            return Forbidden()

        req = Request(environ)

        if req.host:
            toks = req.host.split(':')
            _namespace = toks[0]
        else:
            _namespace = None

        req.namespace = _namespace

        try:
            ret = getattr(self, '_handle_%s' % req.method)(req, _namespace, req.path)
        except HTTPException, e:
            return e

        accept = req.headers['Accept']
        if accept == 'application/json':
            response = Response(encode_json(ret))
            response.headers['Content-Type'] = 'application/json'
            return response

        elif accept == 'plain/text':
            response = Response(encode_json(ret, indent=True))
            response.headers['Content-Type'] = 'plain/text'
            return response

        else:
            response = Response(encode_json(ret, indent=True))
            response.headers['Content-Type'] = accept
            return response


    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)(environ, start_response)
