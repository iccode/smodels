""" Resource registry implements the functionality of the resources management.

"""
from collections import namedtuple
import json
import re
from types import FunctionType
import inflect
from . import ResourceNotFoundError, UnsupportedActionError
from sqlalchemy import or_, not_
from sqlalchemy.orm import Query
from smodels.utils import ParamsDict

inflector = inflect.engine()

RESOURCES = {}

no_internal = lambda x: x[0:2] != '__' and x[:2] != '__'
no_special = lambda x: x[0] != '_' and x[-1] != '_'


def _make_name(nm):
    """
    Makes a resource name from a classname. As PEP compliant classes are named
    in capital camecase we do the following transformation
    HistoryItem -> history_items

    Note that the last word is capitalized denoting that we give PLURAL names
    to the resources
    """
    words = re.findall('[A-Z][^A-Z]*', nm)
    if len(words) == 0:
        return inflector.plural(nm.lower())

    words[-1] = inflector.plural(words[-1])
    return "_".join([w.lower() for w in words])


def _make_single_name(nm):
    """
    Makes a singular name from a resource name.
    """
    words = nm.split('_')
    if len(words) == 1:
        return inflector.singular_noun(words[0])
    words[-1] = inflector.singular_noun(words[-1])
    return "_".join(map(str, words))


def _make_bind_request(class_):
    """
    Creates a method that bind a request to an instance
    :param class_:
    :type class_:
    :return:
    :rtype:
    """

    def bind_request(request):
        i = class_()
        kw = dict(request.args)
        if request.headers['Content-Type'] == 'application/json':
            kw.update(json.loads(request.get_data()))
        else:
            kw.update(request.form)

        i.bind_dict(ParamsDict(kw))
        return i

    return bind_request


def _make_search(class_):
    """ Creates a search method if one does not exist in the class.
        The search method is way to perform queries given a web request

    """

    def search(request, **kwords):
        kw = dict(request.args)
        kw.update(request.form)
        kw.update(kwords)

        q = class_.query
        if 'predicates' in kw and isinstance(kw['predicates'], list):
            #if we have supplied predicates the get to run first
            for pred in  kw['predicates']:
                key, operator, value = pred.split('|')
                if not hasattr(class_, key): continue
                prop = getattr(class_, key)

                if operator=="==":
                    q = q.filter(prop == value)
                if operator==">":
                    q = q.filter(prop > value)
                if operator=="<":
                    q = q.filter(prop < value)
                if operator=="<=":
                    q = q.filter(prop <= value)
                if operator==">=":
                    q = q.filter(prop >= value)
                if operator=="<>":
                    q = q.filter(not_(prop == value))

            return q

        for key, value in kw.iteritems():
            if not hasattr(class_, key): continue
            prop = getattr(class_, key)
            if isinstance(value, list):
                clause = or_(*[prop == v for v in value])
                q = q.filter(clause)
            else:
                q = q.filter(prop == value)
            pass

        return q

    return search


class Resource(object):
    """
    Class representing a resource
    """

    def __init__(self, class_):
        assert isinstance(class_, type)

        cname = class_.__name__
        self.name = class_.__resource__.get('name', _make_name(cname))
        self.single = _make_single_name(self.name)

        # we mutate the original name or add the new one
        class_.__resource__['name'] = self.name

        resource = class_.__resource__

        def try_id(id_):
            if hasattr(class_, id_):
                self.id = id_
                return True

            return False

        for id_ in ['id', 'key', 'code']:
            if try_id(id_): break

        self.base_url = resource.get('url', '/%s/' % self.name)
        self.load = resource.get('load', class_.load)
        self.all = resource.get('all', class_.all)
        if hasattr(class_, 'search'):
            self.search = class_.search
        else:
            self.search = resource.get('search', _make_search(class_))

        self.delete = resource.get('delete', class_.delete)

        if 'bind_request' in resource:
            self.bind_request = resource['bind_request']
        elif hasattr(class_, 'bind_request') and isinstance(class_, FunctionType):
            self.bind_request = class_.bind_request
        else:
            self.bind_request = _make_bind_request(class_)

        self.type = class_
        self.collections = self.__get_collections(class_)
        self.actions = self.__get_actions(class_)

    def representation(self, view='default'):
        return {
            a: getattr(self, a) for a in
            filter(lambda x: no_internal(x) and no_special(x), dir(self))
            if not callable(getattr(self, a))
        }

    def create(self, key=None, **kw):
        t = self.type()
        if key is not None: setattr(t, self.id, key)
        return t

    @staticmethod
    def __get_collections(class_):
        """
        Returns the resources collections under this class
        """
        colls = {}
        for a in filter(no_internal, dir(class_)):
            attr = getattr(class_, a)
            if hasattr(attr, '_collection'):
                colls[a] = getattr(attr, '_collection')

        return colls

    @staticmethod
    def __get_actions(class_):
        """
        Returns the actions supported by this resource
        """
        actions = {}
        for a in dir(class_):
            attr = getattr(class_, a)
            if callable(attr) and hasattr(attr, "_action"):
                actions[a] = getattr(attr, '_action')

        return actions


# region Things returned by find_resource
class _CollectionInstance(namedtuple("CollectionInstance", ['resource',
                                                            'instance',
                                                            'collection',
                                                            'parent',
                                                            'namespace'])):
    def create_item(self, request, key=None):
        res = RESOURCES[self.resource]

        try:
            auto = res.type._has_auto_key()
        except:
            auto = hasattr(res.type, '_autoid')

        if not auto and key is None:
            raise UnsupportedActionError(
                "Cannot bind %s because key is required" % res.name)

        if auto and key is not None:
            raise UnsupportedActionError(
                "Cannot bind %s with key %s because has autoid" % (res.name, key))

        # check if there is an add_<resource> and invoke it
        add_method = 'add_%s' % self.collection
        if hasattr(self.instance, add_method):
            add = getattr(self.instance, add_method)
            if not callable(add):
                raise Exception("Collection adder is not a callable")

            res = add(self.instance, request)
            if self.namespace:
                if hasattr(res, 'namespace'):
                    res.namespace = self.namespace
            return res

        # try to make the resource and call the append method of the collection
        col = getattr(self.instance, self.collection)
        if hasattr(col, 'append'):
            add = getattr(col, 'append')
            if not callable(add):
                raise Exception("Collection added is not callable")

            res = res.bind_request(request)
            if self.namespace:
                if hasattr(res, 'namespace'):

                    res.namespace = self.namespace
            print res
            add(res)

            return res

        raise UnsupportedActionError(
            "Don't know how to add to collection %s" % self.collection)

    def get_item(self, key):
        return getattr(self.instance, self.collection)(key=key)

    def get_all(self):
        collection = getattr(self.instance, self.collection)
        return collection() if callable(collection) else collection


class _ResourceInstance(namedtuple("ResourceInstance", ["resource",
                                                        "instance",
                                                        "isNew",
                                                        "parent",
                                                        "namespace"])
):
    pass


class _ActionInstance(namedtuple("ActionInstance", ["resource",
                                                    "instance",
                                                    "action",
                                                    "parent",
                                                    "namespace"])):
    def invoke(self, *args, **kw):
        method = getattr(self.instance, self.action)

        res = method(*args, **kw)

        return res


#endregion


def register(class_):
    resource = Resource(class_)
    RESOURCES[resource.name] = resource


def find_resource(path, create_missing=False, _namespace=None):
    """
    The actual mechanism for resolving a resource from its path

    """

    def _find(head, rest, parent=None):
        inEnd = len(rest) == 0

        if parent is None:
            if not head in RESOURCES:
                raise ResourceNotFoundError(head)

            parent = RESOURCES[head]
            if inEnd:
                return parent
            else:
                return _find(rest[0], rest[1:], parent)

        if isinstance(parent, Resource):
            inst = parent.load(head)
            if _namespace:
                if hasattr(inst, 'namespace') and inst.namespace != _namespace:
                    inst = None

            if inst is None and not create_missing:
                raise ResourceNotFoundError(path)

            elif inst is None and create_missing:
                inst = parent.create(key=head, namespace=_namespace)
                res = _ResourceInstance(resource=parent.name,
                                        instance=inst,
                                        isNew=True, parent=parent,
                                        namespace=_namespace)
            else:
                res = _ResourceInstance(resource=parent.name,
                                        instance=inst,
                                        isNew=False, parent=parent,
                                        namespace=_namespace)

            if inEnd:
                return res
            else:
                return _find(rest[0], rest[1:], res)

        elif isinstance(parent, _ResourceInstance):
            resource = RESOURCES[parent.resource]
            if head in resource.collections:
                instance = _CollectionInstance(
                    resource=resource.collections[head].of,
                    instance=parent.instance,
                    collection=head,
                    parent=parent,
                    namespace=_namespace)

                if inEnd:
                    return instance
                else:
                    return _find(rest[0], rest[1:], instance)
            elif head in resource.actions and inEnd:
                instance = _ActionInstance(
                    resource=resource,
                    instance=parent.instance,
                    action=head,
                    parent=parent,
                    namespace=_namespace
                )
                return instance

            raise UnsupportedActionError(path)

        elif isinstance(parent, _CollectionInstance):
            item = parent.get_item(head)

            #we can only create the last component of the URI
            if inEnd and create_missing and item is None:
                resource = RESOURCES[parent.resource]
                res = _ResourceInstance(
                    resource=parent.resource,
                    instance=parent.create_item(head),
                    isNew=True,
                    parent=parent
                )
                return res

            if item is None:
                raise ResourceNotFoundError(path)

            res = _ResourceInstance(resource=parent.resource,
                                    instance=item, isNew=False,
                                    parent=parent, namespace=_namespace)

            if inEnd:
                return res
            else:
                return _find(rest[0], rest[1:], res)

        assert False  # We shouldn't be here

    toks = path.split('/')
    return _find(toks[0], toks[1:])


def get(path, request):
    """
    Get a resource by it's path
    """
    path_toks = filter(lambda x: len(x), path.split('/'))
    if not len(path_toks):
        return RESOURCES.values()

    resource = find_resource("/".join(path_toks), _namespace=request.namespace)
    if resource is None:
        raise ResourceNotFoundError(path)
    elif isinstance(resource, Resource):
        if not request.args:
            q = resource.all()
            print "Instance is ", type(q)
            if hasattr(resource.type, 'namespace') and isinstance(q, Query):
                q = q.filter(resource.type.namespace == request.namespace)

            return q.all()
        else:
            return resource.search(request).all()

    elif isinstance(resource, _CollectionInstance):
        return resource.get_all()
    elif isinstance(resource, _ResourceInstance):
        return [resource.instance]
    else:
        return resource


def put(path, request):
    """
    Create a new resource in the path
    """
    path_toks = filter(lambda x: len(x), path.split('/'))
    if not len(path_toks):
        raise UnsupportedActionError("Cannot post in root")

    resource = find_resource("/".join(path_toks), create_missing=True, _namespace=request.namespace)
    if resource is None:
        raise ResourceNotFoundError(path)
    elif isinstance(resource, _CollectionInstance):
        r = resource.create_item(request)
        return r, False
    elif isinstance(resource, _ResourceInstance):
        resource.instance.bind_request(request)
        return resource

    raise UnsupportedActionError("Don't know what to do with %s" % path)


def delete(path, request):
    """
    Deletes a resource at the path
    """
    path_toks = filter(lambda x: len(x), path.split('/'))

    if not len(path_toks):
        raise UnsupportedActionError("Cannot delete in root")

    resource = find_resource("/".join(path_toks), _namespace=request.namespace)
    if resource is None:
        raise ResourceNotFoundError(path)
    elif isinstance(resource, _CollectionInstance):
        raise UnsupportedActionError("Cannot delete at uri %s" % path)
    elif isinstance(resource, _ResourceInstance):
        print "Deleting resource"
        return resource.instance

    raise UnsupportedActionError("Don't know what to do with %s" % path)


def post(path, request):
    """
    Posts a resource to a url
    """
    path_toks = filter(lambda x: len(x), path.split('/'))
    if not len(path_toks):
        raise UnsupportedActionError("Cannot post in root")

    resource = find_resource("/".join(path_toks), _namespace=request.namespace)
    if resource is None:
        raise ResourceNotFoundError(path)
    elif isinstance(resource, Resource):
        r = resource.bind_request(request)
        if hasattr(r, 'namespace'):
            r.namespace = request.namespace
        return r, True
    elif isinstance(resource, _CollectionInstance):
        r = resource.create_item(request)
        return r, False
    elif isinstance(resource, _ActionInstance):
        result = getattr(resource.instance, resource.action)(**request.args)
        return result, False

    raise UnsupportedActionError("Don't know what to do with %s" % path)
