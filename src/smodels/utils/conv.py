import datetime
import dateutil.parser
import dateutils
from i18n.dates import parse_datetime, parse_date, parse_time

TRUTH_VALUES = {'on', 'true', '1', 'checked'}


def p_int(str, default=0):
    """Parses an integer from a string, and if fails, returns a default"""
    if str is None or len(str) == 0: return default
    try:
        return int(str)
    except:
        if default: return default
        raise


def p_float(str, default=0.0):
    """Parses a float from a string, and if fails, returns a default"""
    if isinstance(str, (float, int)): return str
    if str is None or len(str) == 0.0: return default
    try:
        str = str.replace(u'\u20ac', '').replace(' ', '')
        if '.' in str and ',' in str:
            str = str.replace('.', '').replace(',', '.')
        if ',' in str and not '.' in str:
            str = str.replace(',', '.')
        if '.' in str and not ',' in str and str.count('.') > 1:
            while str.count('.') > 1:
                idx = str.index('.')
                str = str[0:idx - 1] + str[idx + 1:]

        return float(str)
    except:
        if default: return default
        raise


def p_bool(str, default=False):
    """Parses a bool from a string, and if fails, returns a default. Truth values are on/true/1/checked"""
    if str is None or len(str) == 0.0: return default

    if str.lower() in TRUTH_VALUES:
        return True

    return False


def p_date(str, default=None, locale=None):
    """Parses a string to a date.

    :param str: String to parse
    :param format: A format string to use
    """
    if isinstance(str, (datetime.datetime, datetime.date)): return str
    if str is None or len(str) == 0.0: return default
    try:
        return parse_date(str, locale=locale) if locale else parse_date(str)
    except:
        return default


def p_time(str, default=None, locale=None):
    if isinstance(str, (datetime.datetime, datetime.date)): return str
    if str is None or len(str) == 0.0: return default
    try:
        return parse_time(str, locale=locale) if locale else parse_time(str)
    except:
        return default

def p_datetime(str, default=None, locale=None):
    if isinstance(str, (datetime.datetime, datetime.date)): return str
    if str is None or len(str) == 0.0: return default
    try:
        return parse_datetime(str, locale=locale) if locale else parse_datetime(str)
    except:
        return dateutil.parser.parse(str)
