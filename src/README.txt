Smodels is a wrapper over sqlalchemy etc that provides

1) Namespace support (for multitenant instances)
2) A better session management
3) A more concise API for oft-used functionality
4) A REST-like API for models