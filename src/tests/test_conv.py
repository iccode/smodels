import datetime
from smodels.utils import ParamsDict
from smodels.utils.conv import p_date, p_time, p_datetime


def test_p_date():
    date = p_date('10/05/2004', locale='en_GB')
    assert datetime.date(2004, 05, 10) == date

    date = p_date('10/05/2004', locale='el_GR')
    assert datetime.date(2004, 05, 10) == date

    date = p_date('10/05/2004')
    assert date == datetime.date(2004, 05, 10)


    date = p_date('10/05/2004', locale='en_US')
    assert datetime.date(2004, 10, 05) == date



def test_p_time():
    time = p_time('13:12', locale='en_GB')
    assert datetime.time(hour=13, minute=12)==time


def test_p_datetime():
    time = p_datetime('01/05/2004 13:12', locale='en_GB')
    assert datetime.datetime(2004, 05, 01, hour=13, minute=12)==time

    time = p_datetime('01-05-2004 13:12', locale='en_GB')
    assert datetime.datetime(2004, 05, 01, hour=13, minute=12)==time

    time = p_datetime('01/05/2004 13:12', locale='en_US')
    assert datetime.datetime(2004, 01, 05, hour=13, minute=12)==time


    dt = datetime.datetime.now()
    print str(dt)
    dt_parsed = p_datetime(str(dt), locale= 'en_GB')
    assert dt_parsed==dt

def test_params_dict():

    d = {
        'int_key': '1',
        'float_key': '1.1',
        'date_key': '2012-01-01'
    }

    pd = ParamsDict(d)

    assert pd.int('int_key')==1

    popped_float = pd.pop_float('float_key')

    assert popped_float==1.1
    assert not 'float_key' in pd

    assert 100.1 == pd.pop_float('float_key', 100.1)