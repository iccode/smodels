from smodels.resources.resource_registry import _make_name

__author__ = 'flatline'

def test_make_name():
    assert _make_name("Action") == "actions"
    assert _make_name("HistoryItem") == "history_items"
    assert _make_name("HistoryItemsComposer") == "history_items_composers"

