from smodels.resources import registry
import pytest

import resources_mocks
from smodels.resources import UnsupportedActionError

__author__ = 'flatline'

def test_get():

    assert len(registry.get('/')) == 3

    path = "cases/123/actions"
    res  = registry.get(path)

    assert isinstance(res, list)
    assert res[0].case.id=='123'

    path = "cases/123/actions/1"
    res = registry.get(path)

    assert res.id=="1"

    path = "/cases"
    res = registry.get(path)

    assert len(res)==10

def test_put():

    path = "cases/123/actions/1"
    res = registry.put(path, name=2)

    assert res.name==2

    path = "cases/123/actions/666"
    res = registry.put(path, name=2)

    assert res.name==2
    assert res.id=="666"

    with pytest.raises(UnsupportedActionError):
        path = "cases/123/actions"
        res = registry.put(path, name=3)

def test_post():

    path = "cases/"
    res, _ = registry.post(path, id=2, name=2)

    assert res.name==2

    path = "/cases/123/actions"
    res, _ = registry.post(path, key=5, title="Test")

    assert res.id == 5

    path = "cases/123/actions/1/autoids/"
    res,_ = registry.post(path, name=2)

    assert res.name==2
    assert res.id > 0

    with pytest.raises(UnsupportedActionError):
        path = "cases/123"
        res = registry.post(path, name=3)

