import json
from threading import Thread
import requests
import smodels
from smodels.sa import SAEntity
from smodels.session import DbSession, get_engine
import time

import resources_sa
from fixtures import engine

smodels.NS_PROVIDER = lambda: 'localhost'

#region Setup

import random
from smodels.resources.wsgi_web import ResourcesApp
from utils import a_number

port = random.randint(10000, 11000)

def _start_server():
    import cherrypy
    import resources_sa

    cherrypy.config.update({'server.socket_port':port})
    cherrypy.tree.graft(ResourcesApp(), '/res')
    cherrypy.engine.start()
    cherrypy.engine.block()

def setup_module(module):
    engine = get_engine()
    SAEntity.metadata.create_all(bind=engine)
    engine.dispose()
    module.proc = Thread(target=_start_server)
    module.proc.start()
    time.sleep(1)


def teardown_module(module):
    import cherrypy
    cherrypy.engine.exit()
    module.end = True

    # engine = get_engine()
    # for t in SAEntity.metadata.sorted_tables:
    #     engine.execute('DROP TABLE %s CASCADE'%t.description)
    # engine.dispose()

#endregion
def _u(part):
    return "http://localhost:%d/res%s"%(port, part if part[0]=='/' else '/'+part)

#TODO: Add test for posting to a resource that has a add_<resource> method

def test_get(engine):
    with DbSession(engine = engine) as sess:
        for i in range(0,10):
            c = resources_sa.Case()
            c.title = "Me %d"%i
            sess.add(c)
            sess.commit()

    r = requests.get(_u('/cases'))
    print r.text
    j = r.json()

    assert len(j)==10
    assert j[0]['title'] == "Me 0"

    r = requests.get(_u(j[0]['resURL']))
    j = r.json()
    assert isinstance(j[0], dict)
    assert j[0]['title'] == "Me 0"

def test_get_unknown():
    r = requests.get(_u('/cases/myself'))
    assert r.status_code == 404

def test_get_resource_collection(engine):
    with DbSession(engine=engine) as sess:
        c = resources_sa.Case(title='test_case')
        sess.add(c)

        times = a_number(1,10)
        for i in range(0,times):
            c.actions.append(resources_sa.Action(title='test_action_%d'%i))
        sess.commit()

        r = requests.get(_u('/cases/%d/actions'%c.id))

    res = r.json()
    assert len(res)==times
    for i in range(0,times):
        res[i]["title"] = "test_action_%d"%i


def test_post(engine):
    requests.post(_u("/cases"), data=json.dumps({
        'title':'Tasos'
    }), headers={'Content-Type': 'application/json'})

    r = requests.get(_u('/cases'))
    res = r.json()
    assert len(res)==1
    assert res[0]['title'] == 'Tasos'

def test_post_on_collection(engine):
    with DbSession(engine=engine) as sess:
        c = resources_sa.Case(title='test_case')
        sess.add(c)
        sess.commit()
        id = c.id

    times = a_number(1,10)

    for i in range(0,times):
        r = requests.post(_u('/cases/%d/actions/'%id), data={
            'title':'test_action_%d'%i
        })


    c = resources_sa.Case.load(c.id)
    assert len(c.actions)==times

def test_post_on_action(engine):
    with DbSession(engine=engine) as sess:
        c = resources_sa.Case(title='hello world')
        c.actions.append(resources_sa.Action(title='hello action'))
        sess.add(c)
        sess.commit()

        r = requests.post(_u(c.resURL+"/clear_actions"))

        assert r.text == '"Ok"'
        c = resources_sa.Case.load(c.id)
        assert len(c.actions)==0


def test_post_on_action_with_params(engine):
    with DbSession(engine=engine) as sess:
        c = resources_sa.Case(title='hello world')
        c.actions.append(resources_sa.Action(title='hello action'))
        sess.add(c)
        sess.commit()

        r = requests.post(_u(c.resURL+"/clear_actions?answer=me"), headers={
            'Content-Type':'application/json'
        })

        assert r.text == '"me"'
        c = resources_sa.Case.load(c.id)
        assert len(c.actions)==0

def test_post_on_required_key():

    r = requests.post(_u('/customers'), data=json.dumps({
        'name':'Tasos'
    }), headers= {'Content-Type': 'application/json'})

    assert  r.status_code==409

def test_post_on_missing():
    r = requests.post(_u('/customers/123'), data=json.dumps({
        'name':'Tasos'
    }), headers={'Content-Type': 'application/json'})

    assert  r.status_code==404


def test__put_with_no_key():

    r = requests.put(_u('/customers'), data=json.dumps({
        'name':'Tasos'
    }), headers={'Content-Type': 'application/json'})

    assert  r.status_code==403

def test_put(engine):
    with DbSession(engine=engine) as sess:
        customer = resources_sa.Customer(fiscal="1234", name="asdasd")
        sess.add(customer)
        sess.commit()

        assert customer is not None

    r = requests.put(_u('/customers/1234'),data=json.dumps({
        'name':'Tasos'
    }), headers= {'Content-Type':'application/json'})

    with DbSession(engine=engine) as sess:
        customer = resources_sa.Customer.load('1234')
        assert customer.name == "Tasos"

def test__put_on_autokey():
    r = requests.put(_u('/cases'), data=json.dumps({
        'title':'test'
    }), headers={'Content-Type': 'application/json'})

    assert r.status_code == 403

def test_delete(engine):
    times = a_number(1)

    with DbSession(engine=engine) as sess:
        for i in range(0,times):
            sess.add(
                resources_sa.Case(title="test_%d"%i)
            )
        sess.commit()


    r = requests.get(_u('/cases'))
    res = r.json()
    assert len(res) == times

    for r in res:
        requests.delete(_u(r['resURL']))

    r = requests.get(_u('/cases'))
    res = r.json()

    assert len(res)==0


def test_namespace_get(engine):
    with DbSession(engine=engine) as sess:
        sess.add(
            resources_sa.Site(namespace='test.gr', title='test')
        )
        sess.add(
            resources_sa.Site(namespace='local.iccode.gr', title='local')
        )
        sess.add(
            resources_sa.Site(namespace='localhost', title='localhost')
        )
        sess.commit()

    r = requests.get(_u('/sites'))
    res = r.json()

    assert len(res)==1


def test_namespace_post(engine):
    with DbSession(engine=engine) as sess:
        sess.add(
            resources_sa.Site(namespace='test.gr', title='test')
        )
        sess.add(
            resources_sa.Site(namespace='local.iccode.gr', title='local')
        )
        sess.add(
            resources_sa.Site(namespace='localhost', title='localhost')
        )
        sess.commit()

    r = requests.post(_u('/sites'), data=json.dumps({
        'title':'test'
    }), headers={'Content-Type': 'application/json'})

    print r.reason

    assert r.status_code == 200

    r = requests.get(_u('/sites'))
    res = r.json()

    assert len(res)==2


def test_search(engine):
    with DbSession(engine = engine) as sess:
        for i in range(0,10):
            c = resources_sa.Case()
            c.title = "Me %d"%i
            sess.add(c)
            sess.commit()


    r = requests.get(_u('/cases?title=Me 1'))

    assert len(r.json())==1
