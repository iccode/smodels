import smodels
from smodels.session import get_engine, DbSession
from smodels.sa import SAEntity
import time

from fixtures import engine

from dogpile.cache import make_region
from . import resources_sa

smodels.CACHE_REGIONS['models'] = make_region().configure(
    'dogpile.cache.redis',
    arguments=dict(
        host='localhost',
        port=6379,
        db=1,
        redis_expiration_time=60 * 60,  #an hour
        distributed_lock=True
    )
)

def setup_module(module):
    engine = get_engine()
    SAEntity.metadata.create_all(bind=engine)
    engine.dispose()


def teardown_module(module):
    pass


def test_caching_get(engine):
    with DbSession(engine=engine) as sess:
        sess.add(
            resources_sa.Site(namespace='test.gr', title='test')
        )
        sess.add(
            resources_sa.Site(namespace='local.iccode.gr', title='local')
        )
        sess.add(
            resources_sa.Site(namespace='localhost', title='localhost')
        )
        sess.commit()

    s = time.time()
    with DbSession(engine=engine):
        q = resources_sa.Site.cached_query('models')
        a = q.all()
        q.invalidate()
    e = time.time()

    t1 = e - s
    print "First       ", t1

    s = time.time()
    with DbSession(engine=engine):
        q = resources_sa.Site.cached_query('models')
        a = q.all()
    e = time.time()

    t2 = e - s
    print "Invalidated ", t2

    s = time.time()
    with DbSession(engine=engine):
        q = resources_sa.Site.cached_query('models')
        q.all()
    e = time.time()
    t3 = e - s
    print "Cached      ", t3

    s = time.time()
    with DbSession(engine=engine):
        q = resources_sa.Site.cached_query('models').all()
    e = time.time()

    t4 = e - s
    print "Second      ", t4

    assert t1 > t4

    with DbSession(engine=engine):
        q = resources_sa.Site.cached_query('models').all()
        q2 = resources_sa.Site.cached_query('models').filter(resources_sa.Site.id==5).all()
