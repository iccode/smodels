import json
from threading import Thread
import requests
import smodels
from smodels.sa import SAEntity
from smodels.session import DbSession, get_engine
import time

import resources_sa
from fixtures import engine

smodels.NS_PROVIDER = lambda: 'localhost'

#region Setup

import random
from smodels.resources.wsgi_web import ResourcesApp
from utils import a_number

port = random.randint(10000, 11000)

def _u(part):
    return "http://localhost:%d/res%s"%(port, part if part[0]=='/' else '/'+part)

def _start_server():
    import cherrypy
    import resources_sa

    def authenticate(env):
        print env
        if 'HTTP_X_AUTHENTICATE' not in env:
            return False

        return env['HTTP_X_AUTHENTICATE'] == 'x'

    cherrypy.config.update({'server.socket_port':port})
    cherrypy.tree.graft(ResourcesApp(pre_hook=authenticate), '/res')
    cherrypy.engine.start()
    cherrypy.engine.block()

def setup_module(module):
    engine = get_engine()
    SAEntity.metadata.create_all(bind=engine)
    engine.dispose()
    module.proc = Thread(target=_start_server)
    module.proc.start()
    time.sleep(1)


def teardown_module(module):
    import cherrypy
    cherrypy.engine.exit()
    module.end = True

def test_authenticate_hook():
    r = requests.get(_u('/cases'), headers={
        'X-Authenticate': 'x'
    })

    assert r.status_code == 200

    r = requests.get(_u('/cases'), headers={
        'X-Authenticate': 'x2'
    })


    assert r.status_code == 403

