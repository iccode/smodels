import pytest
from sqlalchemy import create_engine
from smodels.sa import SAEntity
from smodels.session import get_engine
import resources_sa
import tests


@pytest.fixture()
def engine(request):
    engine = get_engine()

    con = engine.connect()
    with con.begin() as trans:
        for table in reversed(SAEntity.metadata.sorted_tables):
            con.execute(table.delete())
        trans.commit()
    con.close()
    def _clear_all():
        engine.dispose()

    request.addfinalizer(_clear_all)
    return engine
